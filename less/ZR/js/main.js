$(function()
{
    const swiper = new Swiper('.swiper-container', 
    {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
  
        // If we need pagination
        pagination: {
          el: '.swiper-pagination',
        },
  
        // Navigation arrows
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
  
        // And if we need scrollbar
        //scrollbar: {
        //  el: '.swiper-scrollbar',
        //},
      });

      $( "#accordion" ).accordion(
        {
           collapsible:true,
           heightStyle:"content"
        }
      );

      $('.section3__li').click(function(j) {
        var $this = $(this);

        if (!$this.hasClass("accordion-active")) {
          $('.section3__li').removeClass("accordion-active");
          $('.section3__li-btn').removeClass('section3__li-btn--rotate');
        }
        $this.toggleClass("accordion-active");        
        $('.section3__li-btn',this).toggleClass('section3__li-btn--rotate');
      });

      tabElements = document.querySelectorAll('.section2__atc, .section2__foto');
      tabBtns = document.querySelectorAll('.tabs__btn');

      tabBtns.forEach(function(tabBtn){
        tabBtn.addEventListener('click', function(event){
          const path = event.currentTarget.dataset.path;
          tabElements.forEach(function(elem){  
            if (elem.dataset.target == path) 
              elem.classList.remove('tab--deactivate');
            else
              elem.classList.add('tab--deactivate');     
          })         
          tabBtns.forEach(function(elem){
            if (elem.dataset.path == path) 
              elem.classList.add('btn__active');
            else
              elem.classList.remove('btn__active');          
          })
        }) 
      })
});